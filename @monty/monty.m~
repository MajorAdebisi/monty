classdef monty
    properties
        montyContinue           %condition under which to continue
    end
    
    
    properties (Hidden=true)
        workInterval = 1200;
        elapsedTime  = 0;
        startTime;
        inputs
        numberOfWorkers        %most recent number of parallel workers
    end
    
    
    properties (Hidden=true)
        montyAddField           %parallel processing: fields to add
        montyConcatField        %parallel processing: fields to concatenate
    end
    
    properties (Hidden)
        plotTitle = '';         %legend for automated plotting
    end
    
    
    properties (Constant,Hidden=true)
        configDir      = 'var';          %path for configuartion filename
        configFileName = 'montyConfig';  %configuration filename prefix
        defaultJobname = 'userjob';      %default userjob name
    end
    
    
    %threshold search
    properties (Hidden=true)
        check            %child function sets to 0 if fails, 1 if passes
        searchParam      %search parameter, e.g. SNR
        ilow             %parameter value for which check = 0
        ihigh            %parameter value for which check = 1
        searchResolution %how close high and low need to be before stopping
    end
    
    
    
    methods
        function curve = monty(varargin)
            % Crib sheet of typical monty commands
            %
            % BASIC USE
            %
            % monty.generate('dataHamming',exampleSimulation)
            % monty.generate('dataHamming',exampleSimulation,'ebnodb',{1 2 3})
            % monty.run('dataHamming')
            % monty.run('dataHamming','parallel')
            %
            % load dataHamming
            % exampleSimulation.plot
            %
            % monty.updateContinue('curve.nWordError < 100')
            % curve = curve.extend
            %
            % REMOTE SERVER
            %
            % monty.mkdirVar
            % monty.ping
            % monty.runRemote('dataHamming')
            % monty.fromRemote
            % curve = monty.fromRemote('dataHamming')
            % monty.qstat
            % monty.qdel
            % monty.remoteShell
            %
            % DEBUGGING
            %
            % curve = exampleSimulation('ebnodb',2)
            
            
            %nothing to do!  Subclass constructors do all the work
        end
        
        
        function c = montyErrorLimit(curve,option)
            % MONTY.MONTYERRORLIMIT   Error limit check
            %
            % curve.montyErrorLimit returns true if the error limit
            % condition in curve.montyContinue is false, or if the elapsed
            % time using curve.startTime exceeds curve.workInterval
            %
            % curve.montyErrorLimit('noTimeCheck') or
            % curve.montyErrorLimit('n') skips the time chck
            %
            
            c = eval(curve.montyContinue);
            if c == false
                return
            end
            
            if nargin > 1 && (strcmpi(option,'noTimeCheck') || strcmpi(option,'n'))
                return
            end
            
            if ~isempty(curve.startTime)
                thisElapsedTime = etime(clock,curve.startTime);
                if thisElapsedTime > curve.workInterval
                    c = false;
                    fprintf('Work interval %d reached, elapsed time %0.1f\n',curve.workInterval,thisElapsedTime);
                end
            else
                %fprintf('expecting non-empty curve.startTime');
            end
        end
        
        
        function curve = inputArguments(curve,varargin)
            assert(mod(length(varargin),2) == 0,'Each name requires an value')
            for ii = 1:length(varargin)/2
                assert(ischar(varargin{2*ii-1}),'Names must be strings of object properties');
                curve.(varargin{2*ii-1}) = varargin{2*ii};
            end
        end
        
        
        function cout = postParallelProcessing(curve,t)
            cout = t(1);
            
            addField = curve.montyAddField;
            concatField = curve.montyConcatField;
            
            for kk = 2:length(t)
                for ii = 1:length(addField)
                    if any(strcmp(addField{ii},properties(curve)))
                        cout.(addField{ii}) = cout.(addField{ii}) + t(kk).(addField{ii});
                    else
                        error('Post-parallel combining field %s does not exist.',upper(addField{ii}));
                    end
                end
                
                for ii = 1:length(concatField)
                    if any(strcmp(concatField{ii},properties(curve)))
                        cout.(concatField{ii}) = [cout.(concatField{ii}) , t(kk).(concatField{ii})];
                    else
                        error('Post-parallel combining field %s does not exist.',upper(concatField{ii}));
                    end
                end
            end
        end
        
        
        function t = preParallelProcessing(curve)
            addField = curve.montyAddField;
            concatField = curve.montyConcatField;
            
            %copy curve to each worker t
            t(1:curve.numberOfWorkers) = curve;
            
            %but addFields are set to 0 and concatFields are set to []
            %in worker t(2), t(3),...
            for kk = 2:length(t)
                for ii = 1:length(addField)
                    if any(strcmp(addField{ii},properties(curve)))
                        t(kk).(addField{ii}) = 0;
                    else
                        error('Post-parallel combining field %s does not exist.',upper(addField{ii}));
                    end
                end
                
                for ii = 1:length(concatField)
                    if any(strcmp(concatField{ii},properties(curve)))
                        t(kk).(concatField{ii}) = [];
                    else
                        error('Post-parallel combining field %s does not exist.',upper(concatField{ii}));
                    end
                end
            end
            
        end
        
        
        function ha = plot(curve,xProperty,yProperty)
            %monty.quickPlot  Semi-log Y plot
            %
            %  MONTY.QUICKPLOT(XPROPERTY,YPROPERTY) makes a semi-log Y plot
            %  using XPROPERTY and YPROPERTY for the x-axis and y-axis,
            %  respectively.
            %
            %  HA = MONTY.QUICKPLOT returns the graphic handle.
            %
            %  Typical use:
            %    load('dataHamming')
            %    ha = curve.quickPlot('ebnodb','ber')
            %    ha.Color = [0.8 0.2 0];
            %
            
            numberOfLines = length(get(gca,'Children'));
            
            if isprop(curve(1),'plotvar_x')
                xProperty = curve(1).plotvar_x;
            end
            
            if isprop(curve(1),'plotvar_y')
                yProperty = curve(1).plotvar_y;
            end
            
            xData = [curve.(xProperty)];
            yData = [curve.(yProperty)];
            h     = semilogy(xData,yData);
            
            xlabel(curve(1).plotvar_x);
            ylabel(curve(1).plotvar_y);
            
            lg = legend;
            lg.String{numberOfLines+1} = curve.plotTitle;
            if nargout > 0
                ha = h;
            end
        end
        
        function curve = extend(curve,pos)
            %curve.extend  Adds a new unintialized item the array
            %
            %  curve = curve.extend(M) add a new uninitialized object after
            %  position M.  If N = length(curve), then valid values for M
            %  are 0 to N.
            %
            %  curve = curve.extend takes the default of N, placing the new
            %  at the end of curve.
            %
            %  This function uses MATLAB's eval function.
            
            %this creates a new object of the same class as curve:
            eval(sprintf('c = %s;',class(curve)));
            
            if nargin < 2
                pos = length(curve);
            end
            assert(pos <= length(curve),'pos position too large');
            assert(pos >= 0,'pos must be 0 or greater');
            c.montyContinue = curve(1).montyContinue;
            curve = [curve(1:pos) c curve(pos+1:end)];
            
            fprintf('be sure to update the new array with correct parameters.\n');
        end
        
    end
    
    methods (Static)
        
        
        runRemote(inputString,varargin)
        
        
        status = sendMail(toaddr,subject,message)
        
        
        function generate(filename,curveIn,varargin)
            
            if nargin < 2
                error('minimum two input arguments')
            end
            
            curveIn.inputs = varargin;
            
            %build the object array from structs
            tempStruct = struct(varargin{:});
            [M,N] = size(tempStruct);
            assert(M == 1,'only 1-dimensional inputs are allowed');
            fields = fieldnames(tempStruct);
            curve(1:N) = curveIn;
            for ii = 1:N
                for jj = 1:length(fields)
                    %                     if any(strcmp(fields{jj},fieldnames(curveIn)))
                    %                         curve(ii).(fields{jj}) = tempStruct(ii).(fields{jj});
                    %                     else
                    %                         error('Class %s does not have property %s (case-senstive)',upper(class(curveIn)),upper(fields{jj}));
                    %                     end
                    try
                        curve(ii).(fields{jj}) = tempStruct(ii).(fields{jj});
                    catch me
                        error('Class %s does not have property %s (case-senstive)',upper(class(curveIn)),upper(fields{jj}));
                    end
                end
            end
            
            %Check for local filename before overwriting
            %The EXIST command searches the entire MATLAB path, not the desired behavior.
            filename = monty.cleanFilename(filename);
            d = dir(filename);
            if isempty(d)
                r = 'y';
            else
                fprintf('WARNING: About to overwrite %s. \n',upper(filename));
                r = input('Enter ''y'' to overwrite existing file [y/N] ','s');
            end
            if ~isempty(r) && strcmpi(r(1),'y')
                save(filename,'curve');
                fprintf('Saved to file %s.  Use monty.run(''%s'') to begin simulation.\n',upper(filename),filename);
            else
                fprintf('Did nothing.\n');
                return;
            end
        end
        
        
        function thresholdSearch(filename)
            %could functionalize the filename select in run()
            
            filename = monty.cleanFilename(filename);
            assert(exist(filename,'file') == 2,'File %s does not exist',filename);
            load(filename)
            
            assert(~isempty(curve),'CURVE is empty, nothing to do');
            assert(ismatrix(curve),'multidimensional CURVE is not supported');
            
            %monty.generate(filename,wer_search,'param','SNR','ilow',0,'ihigh',20)
            done = false;
            
            [low,high] = curveThresholdFindLowHigh(curve);
            while (abs(high - low) > curve(1).resolution)
                c = curve(1);
                c.(c.searchParam) = (low + high) / 2;
                
                curve(end+1) = c.simulate;
                
                save(filename,'curve','-v7.3');  %-v7.3 needed for HPCC and some CURVEs?  Why?
                fprintf('saved %s\n',filename);
                [low,high] = curveThresholdFindLowHigh(curve);
            end
        end
        
        function run(filename,parallelFlag)
            %MONTE.RUN    Run the simulation
            %
            %  MONTE.RUN(FILENAME) will run the simulation in FILENAME
            %  FILENAME should be created using MONTY.GENERATE.
            %
            %  MONTE.RUN(FILENAME,'parallel') or MONTE.RUN(FILENAME,'p')
            %  will attempt to open and use the parallel pool. Matlab's
            %  parallel processing toolbox is required.
            %
            %  MONTE.RUN will attempt to run the newest .MAT file in the
            %  current directory.  User is prompted before running.
            %
            %  Brian Kurkoski
            %
            
            if nargin < 2
                parallelFlag = false;
            else
                parallelFlag = strcmpi(parallelFlag,'parallel') || strcmpi(parallelFlag,'p');
            end
            
            %if none specified, find most recent .mat file
            if nargin < 1
                D = dir('*.mat');
                datenum = 0;
                filename = [];
                for ii = 1:length(D)
                    if D(ii).datenum > datenum
                        filename = D(ii).name;
                        datenum  = D(ii).datenum;
                    end
                end
                assert( ~isempty(filename),'no filename specified');
                fprintf('monty.run using %s? ',upper(filename));
                r = input(' [y/N] ','s');
                if isempty(r) || ~strcmpi(r(1),'y')
                    fprintf('Did nothing.\n');
                    return
                end
            end
            
            filename = monty.cleanFilename(filename);
            assert(exist(filename,'file') == 2,'File %s does not exist',filename);
            load(filename)
            
            assert(~isempty(curve),'CURVE is empty, nothing to do');
            assert(ismatrix(curve),'multidimensional CURVE is not supported');
            
            if parallelFlag
                if isempty(gcp('nocreate'))
                    fprintf('creating parallel pool\n');
                end
                myCluster = parcluster();
                if myCluster.NumWorkers >= 12
                    parallelPool = parpool(12);
                else
                    parallelPool = gcp;
                end
                %parallelPool = gcp;
                if ~isempty(parallelPool)
                    fprintf('parallel pool with %d workers.  Use delete(gcp) to delete the parallel pool.\n',parallelPool.NumWorkers)
                else
                    fprintf('failed to create parallel pool\n');
                    parallelFlag = false;
                end
            end
            
            done = false;
            while(~done)
                %Loop over the CURVE array.  Only if it passes over the
                %whole CURVE array without simulating, then we can declare
                %the simulation is done.
                done = true;
                
                for ii = 1:size(curve,1)
                    for jj = 1:size(curve,2)
                        disp('+++ monty + monty + monty + monty + monty +++');
                        if (curve(ii,jj).montyErrorLimit('noTimeCheck') )
                            done = false;
                            
                            curve(ii,jj).startTime = clock;
                            
                            if parallelFlag
                                curve(ii,jj).numberOfWorkers = parallelPool.NumWorkers;
                                
                                %t(1:parallelPool.NumWorkers) = curve(ii,jj);
                                t = curve(ii,jj).preParallelProcessing;
                                
                                parfor worker = 1:parallelPool.NumWorkers
                                    t(worker) = t(worker).simulate;
                                end
                                curve(ii,jj) = curve(ii,jj).postParallelProcessing(t);
                                curve(ii,jj) =  curve(ii,jj).calculate;
                            else
                                curve(ii,jj) = curve(ii,jj).simulate;
                            end
                            
                            curve(ii,jj).elapsedTime = curve(ii,jj).elapsedTime + etime(clock,curve(ii,jj).startTime);
                            curve(ii,jj).startTime   = [];
                            
                            save(filename,'curve','-v7.3');  %-v7.3 needed for HPCC and some CURVEs?  Why?
                            fprintf('saved %s\n',filename);
                        end
                    end
                end
            end
        end
        
        
        function filename = cleanFilename(filename)
            if length(filename) < 5 || ~strcmpi( filename(end-3:end), '.mat')
                filename = [filename '.mat'];
            end
        end
        
        
        function varargout = fromRemote(filename)
            %monty.fromRemote - Copy files from server to local machine
            %
            %   On Mac and Linux systems, CURVEFROMREMOTE downloads files
            %   from the remote server to the local machine.  This command
            %   is not supported on Windows system -- copy files manually.
            %
            %   CURVE = MONTY.FROMREMOTE(FILENAME) will also load FILENAME
            %   and return CURVE in that file.  This file will be loaded
            %   even if it was not one of those copied from the server.
            %
            %   This function requires that configuration variables in
            %   VAR/MONTYCONFIG*.M are set properly.
            %
            
            config = monty.remoteLoadConfiguration;
            
            if ~ispc
                fprintf('Rsync files from %s to local machine\n',upper(config.servername));
                cmd    = sprintf('rsync -ave ssh --update %s@%s:%s/ .',config.username,config.servername,config.remoteDir);
                status = system(cmd);
                if status
                    error('rsync from server failed');
                end
            else
                fprintf('WINDOWS: Copy files from server manually.\n')
            end
            
            if nargin > 0 && nargout > 0
                try
                    data = load(filename);
                    if nargout > 0
                        if isfield(data,'curve')
                            varargout{1} = data.curve;
                        else
                            fprintf('File %s does not contain CURVE',filename);
                            varargout{1} = [];
                        end
                        
                    end
                catch me
                    fprintf('Could not load file %s',filename);
                    varargout{1} = [];
                end
            end
        end
        
        
        function mkdirVar
            %monty.mkdirVar
            
            success = mkdir('var');
            if success
                thispath = mfilename('fullpath');
                ind = strfind(thispath,mfilename);
                thispath = [thispath(1:ind(end-1)-2)];
                pathToConfig = [thispath 'var/montyConfig.m'];
                if exist(pathToConfig,'file')
                    file = fileread(pathToConfig);
                    FID = fopen('var/montyConfig.m','w');
                    if FID
                        fprintf(FID,'%s',file);
                        fclose(FID);
                        fprintf('created var/montyConfig.m.  Now edit var/montyConfig.m manually. \n');
                    else
                        fprintf('Failed to write var/montyConfig.m');
                    end
                else
                    fprintf('could not locate source file var/montyConfig.m');
                end
            else
                error('could not create directory VAR');
            end
            
        end
        
        
        function updateContinue(str)
            %monty.updateContinue(STR)
            %
            %In the most recent MAT file in the current director,
            %updates MONTYCONTINUE for all elements with STR.
            
            if nargin < 1
                fprintf('Error: Continue condition must be specified as a string\n');
                return
            end
            
            [filename,curve,val] = monty.loadLocalFile;
            
            if val
                for ii = 1:length(curve)
                    curve(ii).montyContinue = str;
                end
                save(filename,'curve');
                fprintf('updated file %s\n',filename);
            end
        end
        
        
        function updateTitle(str)
            %monty.updateTitle(STR)
            %
            %In the most recent MAT file in the current director,
            %updates PLOTTITLE for all elements with STR.
            
            if nargin < 1
                fprintf('Error: Plot title must be specified as a string\n');
                return
            end
            
            [filename,curve,val] = monty.loadLocalFile;
            
            if val
                for ii = 1:length(curve)
                    curve(ii).plotTitle = str;
                end
                save(filename,'curve');
                fprintf('updated file %s\n',filename);
            end
        end
        
        
        function result = remoteShell(varargin)
            %monty.remoteShell - Run a shell command on the remote server
            %
            %  Example:
            %      monty.remoteShell('pwd')
            %      monty.remoteShell('qstat -u username')
            %
            %  Brian Kurkoski
            %
            
            if nargin < 1
                error('requires one input argument')
            else
                inputString = '';
                for ii = 1:length(varargin)-1
                    inputString = [inputString varargin{ii} ' '];
                end
                inputString = [inputString varargin{end}];
            end
            
            config = monty.remoteLoadConfiguration;
            
            if ~ispc
                cmd = sprintf('ssh %s@%s "cd %s ; %s"',config.username,config.servername,config.remoteDir,inputString);
                
                [status,result] = system(cmd);
                if status
                    fprintf('Command had no output, or command failed\n');
                end
            else
                error('Windows version not yet tested');
            end
            
            if nargout == 0
                fprintf('%s',result);
                clear result;
            end
        end
        
        
        function ping
            %MONTY.PING  Test remote server
            %
            %  Runs PWD on the remote server as a test to see if remote
            %  commands can be run.
            %
            
            config = monty.remoteLoadConfiguration;
            if ~ispc
                cmd = sprintf('ssh %s@%s "pwd"',config.username,config.servername);
                
                [status,result] = system(cmd);
                if status
                    fprintf('PWD command failed\n');
                else
                    fprintf('Server is %s and PWD is %s\n',config.servername,result);
                end
            else
                error('Windows version not yet tested');
            end
            
        end
        
        
        function qdel(jobnumber)
            %monty.qdel  Delete remote PBS by job number
            %
            %  monty.qdel(23456) deletes PBS job 23456 on the remote
            %  server using:
            %  % qdel 23456
            %
            
            cmd = sprintf('qdel %d',jobnumber);
            monty.remoteShell(cmd);
        end
        
        
        function qstat
            %monty.qstat   Remote PBS jobs owned by user
            %
            %  Using the username in var/montyConfig:
            %  % qstat -u username
            %  is run on the remote PBS server.
            %
            
            config = monty.remoteLoadConfiguration;
            cmd = sprintf('qstat -u %s',config.username);
            monty.remoteShell(cmd);
        end
        
        
        function docs
            %monty.docs
            %
            %on Mac, opens the PDF documentation for monty
            
            if ismac
            thispath = mfilename('fullpath');
            ind = strfind(thispath,mfilename);
            thispath = [thispath(1:ind(end)-1)];
                system(['open ' thispath '../doc/monty.pdf'])
            else
                fprintf('Only on MacOS\n');
            end
        end
        
    end
    
    
    methods (Static,Access = private)
        
        function config = remoteLoadConfiguration
            %monty.remoteLoadConfiguration - Load monty configuration file
            %
            %   This is an internal function. Configuration is for remote
            %   servers.
            %
            
            d = dir;
            t = strcmp({d.name},monty.configDir);
            result = true;
            if ~any(t)
                error('Working directory must contain directory %s/\n',upper(monty.configDir));
            end
            
            t = find(t,1);
            assert(d(t).isdir,'Error: %s must be a directory, not a file',upper(monty.configDir));
            
            d = dir([monty.configDir '/' monty.configFileName '*.m']);
            assert(length(d) > 0, 'Directory %s/ must contain file %s.M',upper(monty.configDir),upper(monty.configFileName));
            assert(length(d) == 1, 'Directory %s/ must contain only one M-file beginning %s*.M. %d files found',upper(monty.configDir),upper(monty.configFileName),length(d));
            
            %Read the file
            tdir = [pwd '/' monty.configDir];
            addpath(tdir);
            command = sprintf('config = %s;',d(1).name(1:end-2));
            eval(command);
            rmpath(tdir);
        end
        
        
        function [filename,curve,val] = loadLocalFile
            %find most recent .mat file
            filename = '';
            curve = [];
            val = 0;
            D = dir('*.mat');
            datenum = 0;
            filename = [];
            for ii = 1:length(D)
                if D(ii).datenum > datenum
                    filename = D(ii).name;
                    datenum  = D(ii).datenum;
                end
            end
            %assert( ~isempty(filename),'no filename specified');
            fprintf('Use %s? ',upper(filename));
            r = input(' [y/N] ','s');
            if isempty(r) || ~strcmpi(r(1),'y')
                fprintf('Did nothing.\n');
                return
            end
            H     = load(filename);
            curve = H.curve;
            val   = 1;
        end
        
        
    end
end