function status = sendMail(toaddr,subject,message)
% monty.sendMail - Send an email message
% 
% MONTY.SENDMAIL(toaddr,subject,message) Sends email MESSAGE to TOADDR with
% SUBJECT.  Three arguments are text strings.  This funciton can be used
% for communicating the simulation progress via email.
%
% STATUS = MONTY.SENDMAIL(...)  STATUS is the status code returned by the
% PHP mail function.  0 indicates the mail was accepted, but does not 
% necessarily indicate the mail will reach the destination. If PHP is not 
% installed, then a warning will be generated and the STATUS is 1.
%
% A simple test if the machine has working PHP mail():
%
%    status = monty.sendMail('user@domain.com','test','test body')
%
% Then check if STATUS is 0. Check that the mail was received.
%
% In this non-working example, curve.montyErrorLimit returns true when the
% simulation has finished:
%
%   curve.nWordError = 10;
%   curve.continue   = 'curve.nWordError < 10';
%   %... simulation here ...
%   if curve.montyErrorLimit
%       msg = sprintf('Finished with %d word errors',curve.nWordError);
%       monty.sendMail('user@domain.com','simulation finished',msg);
%   end
%
% MONTY.SENDMAIL uses local PHP and its mail() function.
%
% See PHP mail() documentation: http://php.net/manual/en/function.mail.php
%
% Brian Kurkoski
%

localPhp = './mailCurve.php';  %temporary file name for php script

assert(nargin > 1,'A destination email address is required')
assert(nargin < 4,'requires 1, 2 or 3 input arguments')
if nargin < 2
    subject = 'no subject';
end
if nargin < 3
    message  = 'An email from Curve Library';
end

[~,phppath] = system('which php');
if isempty(phppath)
    warning('This system does not have php.  Mail not sent.');
    status = false;
    return;
end

FL = fopen(localPhp,'w');

fprintf(FL,'#!%s\n',phppath);
fprintf(FL,'<?php\n');
fprintf(FL,'$toaddr  = "%s";\n',toaddr);
fprintf(FL,'$subject = "%s";\n',subject);
fprintf(FL,'$message = "%s";\n',message);
fprintf(FL,'$status = mail($toaddr,$subject,wordwrap($message,70));\n');
fprintf(FL,'return $status;\n',phppath);

fclose(FL);
system(sprintf('chmod +x %s',localPhp));
status = system(localPhp);

delete(localPhp);

return
