\documentclass[10pt,a4paper]{article}

\usepackage{xspace}
\usepackage[pdftex]{hyperref}

%MATLAB
\usepackage[framed,numbered,autolinebreaks,useliterate]{matlab-prettifier}
\usepackage[T1]{fontenc}  %needed for matlab-prettifier nice font
\lstset{style=Matlab-editor,basicstyle=\mlttfamily}  %defaults
%\lstinputlisting{/Users/kurkoski/pr/bpwb/arimoto/capacityComputationForLectureNotesNoDelete.m}
% \begin{lstlisting}  \end{lstlisting}

\newcommand{\curve}{\texttt{curve}\xspace}
\newcommand{\montylibrary}{Monty\xspace}
\renewcommand{\tt}[1]{\texttt{#1}}

\title{Monty --- A Matlab Library for Managing Communications Simulations}
\author{Brian M.~Kurkoski}

\begin{document}

\maketitle

\montylibrary is a Matlab object-oriented library for efficient simulations of communication systems, particularly generating a curve of  probability of decoder error versus signal-to-noise ratio.  A goal is to obtain statistically reliable data with efficient use of computer time.  If your simulation is written as a \tt{monty} subclass, then \montylibrary is used to begin a simulation, and to stop the simulation when a specified number of errors have occurred.  The Matlab parallel processing toolbox with \tt{parfor}, remote servers and PBS job management software are supported.  \montylibrary allows safely resuming simulations after a server crash.

\montylibrary uses Matlab classes.  It is based on the older Curve Library, which used functions rather than classes. Because of the benefits of a class-based library, development on Curve Library has stopped, and use of \montylibrary is recommended.  Even if you are unfamiliar with Matlab classes, you should be able to write your simulation as a class by modifying the provided examples\footnote{This document uses the terminology of object-oriented programming: a \emph{property} of a class is analogous to a field of a structure, for example it contains a value like SNR.  A \emph{method} of a class is analogous to a function.}.   Communications simulations are an example of the Monte Carlo method, hence the library name ``Monty.''


\section{Introduction}

An error-rate curve shows the decoder error probability versus the signal-to-noise ratio (SNR), and it is a fundamental way to describe the performance of a communication system.  These are obtained by Monte Carlo computer simulations. When generating an error-rate curve, there is a tradeoff between the statistical reliability of your data, and the amount of computer time required to obtain the data. \montylibrary manages this tradeoff using two techniques:
\begin{enumerate}
\item At each SNR value, the simulation terminates when a stopping condition is reached.  Usually the stopping condition is that a fixed number of word errors have occurred.  This stopping condition has the effect of giving good statistical reliability over a range of SNRs.   

\item  Over an SNR range,  \montylibrary  spends a roughly equal amount of time simulating all points in an SNR range that have not yet terminated at their stopping condition.   This is particularly useful in preliminary simulations, when you do not know if your simulation will produce good results, or want to find the interesting SNR range.
\end{enumerate}
This is a practical way to efficiently produce error rate vs.~SNR curves. If your simulation uses the specified interface, \montylibrary can manage simulations.

At low SNRs, the decoder error rate is high, the simulation will terminate quickly as the fixed number of errors is quickly accumulated.  At higher SNRs, the simulation continues until enough errors have accumulated to give the desired statistical reliability.  The statistical reliability is connected with the number of word errors.  The word error limit depends on the application --- typically 10 word errors are sufficient for preliminary data, but 100 to 500 word errors are needed for publication-quality data.  

To obtain statistical reliability which is high and uniform over a range of SNRs, the number of word errors is a good choice as a stopping condition.   \montylibrary spends a fixed amount of time at each SNR point.   When setting up a simulation, the SNR values are specified, for example, 0 dB, 1 dB, 2 dB, 3 dB.  By default, the simulation works on one SNR value for 20 minutes, and then works on the next value for 20 minutes, and so on.  After simulating the last value on the list, it returns to the beginning of the list, continuing cyclically.   If the stopping condition is reached, e.g. if enough word errors have accumulated, this SNR point is considered done, and is dropped from the cycle.

This document assumes a decoder error rate vs. SNR is the objective.  However, other parameters are possible.  A future version of the library may include a noise threshold search.

\montylibrary is partitioned into four levels, of increasing functionality:
\begin{description}
\item[Level 1] Class interface:  a class for communicating with your simulation.
\item[Level 2] Simulation level: efficiently running a simulation over a range of SNRs.
\item[Level 3] Parallel simulations: implement a simulation on a parallel processor.
\item[Level 4] Remote server: run a simulation on a remote server.
\end{description}

The program interface is not very useful by itself, but it implements the necessary interface between your simulation and \montylibrary.  It is required by all other levels.  The simulation level is useful on single-processor computers, and is required for the other two higher levels.  The parallel simulation level is used when a local parallel processing computer is available.   Remote server level is used when a server is available, with or without parallel processing.

\section{Requirements, Downloading and Examples}

\emph{System requirements} \montylibrary works with a variety of versions of Matlab.  For parallel processing using \tt{parfor} on multicore systems, the Parallel Computing Toolbox is required.   For using a remote server, local and remote ssh and rsync are needed; since the development was done using local Mac~OS and remote Linux, this scenario is likely to work best.  For local Windows, there is preliminary support for putty for remote execution, but no support for copying files.

\emph{Installation}  Clone the Gitlab repository:
\begin{lstlisting}
$ git clone https://gitlab.com/bits-lab/monty.git 
\end{lstlisting}
or download from \tt{https://gitlab.com/bits-lab/monty}.  \tt{git clone} creates a large and unneeded \tt{.git} directory.  Remove this using \tt{rm -rf .git}.

%In Matlab, change to this directory and run the startup command to add the library \tt{curve/lib} to the search path:
%\begin{lstlisting}
%>> cd curve                        
%>> startup                           %modifies the search path
%\end{lstlisting}
 
Below are some examples of how \montylibrary is used, which can be run after downloading the library, except for the remote server example, which requires additional setup.

\emph{Example}  A simple Hamming code simulation is \tt{exampleSimulation} which is a class, and a subclass of \tt{monty}.  Using the default parameters, set up a simulation using \tt{monty.generate}, saving to a file \tt{dataHamming.mat}.  Then, perform the simulation on this file using \tt{monty.run}.  After the simulation completes, load the file, and inspect its contents:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation)%setup simulation
>> monty.run('dataHamming')                     %perform simulation
>> load dataHamming.mat           %after sim is complete, load file
>> curve.BER                   %results in an object called curve

ans =

    0.0612                                   %your result will vary
\end{lstlisting}
Setup the simulation using $E_b /N_0 = 4$ dB instead of the default value:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation,'ebnodb',4) 
>> monty.run('dataHamming')
\end{lstlisting}
Modify the above example to  perform the simulation over a range of $E_b/N_0$ values:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation,...
       'ebnodb',{1 2 3 4 5})
>> monty.run('dataHamming')
\end{lstlisting}
\emph{Parallel Processing Example}  \montylibrary will attempt a parallel simulation if the \tt{'parallel'} or \tt{'p'} option is added to \tt{monty.run}:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation); 
>> monty.run('dataHamming','parallel')
\end{lstlisting}
Parallel processing uses the Matlab \tt{parfor} loop.

\emph{Remote Server Example}  Assuming correct configuration (described in Section \ref{sec:remote}), \tt{monty.runRemote} copies files to a remote server and executes the simulation. PBS is parallel computing job management software is supported.
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation,...
            'montyContinue', 'curve.nWordError < 100',...
            'ebnodb',{0 1 2 3 4});
>> monty.runRemote('dataHamming'); %run sim on remote server
\end{lstlisting}

\section{Level 1: Class Interface}

If you write your simulation as a subclass of the \tt{monty} class, as described in this section, your simulation can be managed by the \tt{monty} methods.  The \tt{monty} class definition handles the input parameters and output results to and from your simulation, as well as terminating the simulation with a stopping condition.  

A key point is that your simulation is a class, for example \tt{exampleSimulation}.  This is a subclass of the \tt{monty} class.  The inputs and outputs to your simulation are properties of the \tt{exampleSimulatiom} object. In addition \tt{monty} defines some properties.  An array of \tt{exampleSimulation} objects has a different parameter value, typically SNR. This array is usually called \tt{curve}.  

Using the interface, your simulation can be called as follows. Suppose you have written a function called \tt{exampleSimulation} which implements your simulation. The input arguments are a series of property-value pairs, for example:
\begin{lstlisting}
>> curve = exampleSimulation('ebnodb',2); 
\end{lstlisting}
Multiple property-value pairs \tt{curve = exampleSimulation('k',3,'ebnodb',2);} can be specified.

An example \tt{exampleSimulation.m} is in the source distribution and in Figs.~\ref{fig:sim1}--\ref{fig:sim2} on pages \pageref{fig:sim1} and \pageref{fig:sim2}. Key parts of this example are as follows. The class definition implementing the simulation \tt{exampleSimulation.m} should begin like:
\begin{lstlisting}
classdef exampleSimulation < monty
    properties
        %inputs
        ebnodb     = 1;  
        n          = 7; 
        
        %outputs
        nWordError = 0;
        nWord      = 0;
        WER
    end
\end{lstlisting}
Here, \tt{< monty} indicates \tt{exampleSimulation} is a subclass of \tt{monty}. Properties are both ``inputs'' with their default values and ``outputs'' which do not necessarily have default values.  Counters like \tt{nWordError} are initialized to \tt 0. % so that they can be incremented, that is \tt{nWordError = nWordError + 1} does not give an error. 

The object constructor is where initialization occurs.  It should set parameters for \montylibrary:
\begin{lstlisting}
function curve = exampleSimulation
    %EXAMPLESIMULATION   monty example simulation
    %  Put help text here.
            
    %continue condition: if string evaluates true, then the
    %simulation will continue.  Otherwise, the simulation stops
    curve.montyContinue = 'curve.nWordError < 10';

    %process the input arguments, for example allows
    %exampleSimulation('n',15) to override default 'n' value
    curve = curve.inputArguments(curve,varargin{:});
     \end{lstlisting}

Your simulation will continue until a stopping condition is satisfied.  The stopping condition is specified as a string containing Matlab code, in the property \texttt{montyContinue}.   Code in this string must evaluate \tt{true} or \tt 1 if the stopping condition is not satisfied, \tt{false} or \tt 0 if the stopping condition is satisfied.  In the example above, the stopping condition is that 10 word errors have occurred. The string \texttt{'curve.nWordError < 10'} is an example of Matlab code that evaluates either \tt{true} or \tt{false}.   

%Overriding of default property-value pairs  (\tt{exampleSimulation('k',2)}, for example) is performed by \tt{curve.inputArguments}.

In addition, the object constructor should also initialize your simulation:
\begin{lstlisting}
function curve = exampleSimulation
    % ...
    curve.G = [1 0 0 0 1 1 1; 0 1 0 0 1 1 0; ...
            0 0 1 0 1 0 1; 0 0 0 1 0 1 1]';
    for ii = 1:2^curve.k
        u = de2bi(ii-1,curve.k)';
        curve.codebook(:,ii) = curve.G * u;
    end
end
 \end{lstlisting}
 In this example, the initialization sets the generator matrix \tt{G} and generates the complete codebook \tt{codebook}.  Both are properties of \tt{exampleSimulation}.  Note that the initialization is performed when \tt{monty.generate('filename',exampleSimulation)} is invoked.  \emph{Important note} The current version does not support SNR-dependent initialization.  The file should be generated manually rather than using the method \tt{monty.generate}.

The core of your simulation is executed many times, repeating the encoding and decoding, each time using randomly generated data sequences and noise.  This simulation should be inside a method called \tt{simulate} which has \tt{curve} as both input and output.  \tt{curve} is an \tt{exampleSimulation} object.
\begin{lstlisting}
function curve = simulate(curve)
    R             = curve.k / curve.n; 
    curve.varChan = (0.5/(2*R)) * 10^(- curve.ebnodb / 10);

    while (curve.montyErrorLimit)
    
        %SIMULATION CORE
    
    end
end
\end{lstlisting}
The simulation core should be be inside of a \tt{while (curve.montyErrorLimit)} loop. Before attempting each loop, \tt{curve.montyErrorLimit} will evaluate \tt{curve.montyCondition} and stop the simulation if it has been satisfied.  The simulation can also stop temporarily if a time limit has been reached, see the next section.

During your simulation, output parameters are properties of \tt{curve}, and they should be directly modified, for example:
\begin{lstlisting}
curve.nWords = curve.nWords + 1; 
\end{lstlisting}

\section{Level 2: Simulations}

\montylibrary handles simulations over an SNR range, and initially allocates a equal amount of processor time to each value in the SNR range.  As the simulation progresses, high error rate points reach their stopping condition sooner, and \montylibrary allocates processor time only to those points which have not yet reached their stopping condition.  This is effective for initial simulations when the SNR range of interest is not known.  It aims to get uniform statistical reliability over the SNR range.  It uses processor time effectively within the restriction of the desired statistical reliability. 

Data is stored in an array of \tt{exampleSimulation} objects called \curve, with one object for each SNR value.  For example, the SNR values 0 dB, 1 dB, 2 dB, 3 dB are stored in a 1-by-4 \curve array. This array is stored in a .mat file.   When the simulation begins, this file is read.  The data in the file is used to perform the simulation.  Data is saved to this file periodically.  In the event of a server crash, data from the last save is retained.

The \tt{workInterval} property sets the amount of computer time to spend on one SNR point, before stopping and moving to the next SNR point.  The default value of  \tt{workInterveral} is 1200 seconds (20 minutes). The simulation begins by starting your simulation with SNR of 0 dB, and continues until one of two conditions is reached: (1) the stopping condition in \tt{montyContinue} is reached, or (2) \tt{workInterval} seconds have passed.  The results are saved.   Then, the simulation moves to 1 dB, and simulates until the stopping condition or working time is reached, and again saves the data.  Then 2 dB, then 3 dB.  After simulating 3 dB, a maximum of 80 minutes has passed, less if a stopping condition was reached.  Then, the simulation returns to 0 dB.  If the stopping condition for 0 dB is already satisfied, then the 0 dB simulation is not performed, and moves to the 1 dB simulation.   This continues cyclicly until all the stopping conditions for the four points are satisfied. 

As an example, a simulation for the \tt{exampleSimulation} Hamming code would be set up as follows:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation,'ebnodb',{0 1 2 3});
\end{lstlisting}
This saves data in the file \tt{dataHamming.mat}, which contains the \curve array.  The object is constructed by \tt{exampleSimulation.m}.  A \curve array with four elements is created, where the \tt{ebnodb} properties is set to 0, 1, 2 and 3 in each.  

The simulation is performed using:
\begin{lstlisting}
>> monty.run('dataHamming')
\end{lstlisting}
The file \tt{dataHamming.mat} contains the data for the simulation.   This file is loaded before the simulation begins, and the results are saved to this file, both periodically as the simulation progresses and at the end.  

If the server crashes, then \texttt{monty.run('dataHamming')} will resume the simulation from the point of the last periodically saved file.

After the simulation is complete, or even while it is running, data is available in the file.  Monty has a plot method:
\begin{lstlisting}
>> load dataHamming.mat
>> curve.plot('ebnodb','ber') 
\end{lstlisting}
which will make a semi-log plot of bit-error rate versus $E_b/N_0$.   The following is equivalent to:
\begin{lstlisting}
>> load dataHamming.mat
>> semilogy([curve.ebnodb],[curve.ber])
\end{lstlisting}


\section{Level 3: Parallel Processing}

It is relatively easy to perform communications simulations using parallel processing, because each processor runs the same simulation, except that the random number generator seed differs.  There are several points to be careful about when implementing a parallel simulation, including writing parallel code that Matlab will accept.  Another point to be careful about is combining the results from multiple processors after the simulation is complete, and \montylibrary provides support for this.

It is necessary to specify how the various properties will be combined when the \tt{parfor} loop is compete.  The following should be in the object constructor:
\begin{lstlisting}
curve.montyAddField = {'nBitError','nWordError','nWord'};
curve.montyConcatField = {''};
\end{lstlisting}
Property values to be added after the \tt{parfor} loop is compete (for example, the number of words, the number of errors), are specified using \tt{curve.montyAddField}.  Property values to be concatenated (for example to store the random number generator state after failed decoding) are specified using \tt{curve.montyConcatField}.  Values which do not change during the \tt{parfor} loop do not need to a combining method (property values are copied from \tt{curve(1)}).

Convenience calculations like bit-error rate, should be performed after the method \tt{simulate} finishes:
\begin{lstlisting}
%Convenience calculations
curve.BER = curve.nBitError / (curve.nWord * curve.n);
curve.WER = curve.nWordError / curve.nWord ;
\end{lstlisting}

\section{Level 4: Remote Server \label{sec:remote}}

Using the Matlab command line, you can run your simulation on a remote server. Support for PBS multiprocessor management is included.  After setting up necessary configuration, the command:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation); 
>> monty.runRemote('dataHamming')  
\end{lstlisting}
will copy files to the server, and start the simulation on the server.  After your simulation is completed, copy the files back to the local machine, and inspect the results.
\begin{lstlisting}
>> monty.fromRemote
>> load dataHamming.mat
>> curve.ber 

ans =

    0.0598                                   %your result will vary
\end{lstlisting}

On Mac and Linux systems, \montylibrary uses rsync to copy files and ssh to execute commands on the remote server.   On Windows, these commands are not available by default ---  putty is used to execute commands on the remote server, but files must be copied manually.

\montylibrary must be configured correctly.  It is necessary to (1) specify  a working directory tree and (2) setup a configuration file.  The \montylibrary must be on the remote machine, so it is necessary to (3) manage libraries to be copied to the remote server. It is strongly recommended that you (4) configure keychain on your local machine to avoid entering passwords. 

(1) Specify a working directory.  The working directory contains only files related to your simulation.  Using rsync, this directory and all its subdirectories are copied to the server by the \tt{monty.runRemote} command.  On the server side, Matlab will be started in the working directory.  

(2) The directory \tt{var/} must be in the working directory, and it must contain a configuration file  \tt{var/montyConfig.m} or any \tt{var/montyConfig*.m}.  The configuration file included in the distribution is shown in Figure \ref{fig:configRemote}. The commany \tt{monty.mkdirVar} will create \tt{var/} in the current directory, and copy the example config file into \tt{var}. Modify the settings in \tt{montyConfig.m} for your server environment. 

(3) Manage your libraries.  \montylibrary needs to be copied to the server. In addition, your simulation file may require other libraries to be the server.  If \tt{@monty} is the only library, and it is in the working directory, then nothing more is required. However, if you have other libraries, such as \tt{myLib/}, place it in the working directory and put a  \tt{startup.m} file:
\begin{lstlisting}
addpath([pwd '/myLib']);   %add your library to search path
\end{lstlisting}
in the working directory. If \tt{myLib/} has subfolders, use
\begin{lstlisting}
addpath(genpath([pwd '/myLib']));  
\end{lstlisting}
to add  \tt{myLib/} and its subfolders to search path.
When Matlab starts in the working directory on the remote server, it will execute \tt{startup.m}, and add the \tt{myLib/} directory to the Matlab search path\footnote{Alternatively, Matlab may execute the default \tt{startup.m} file, in a different location.   The location depends on the server operating system and other settings, read  \href{http://www.mathworks.com/help/matlab/matlab_env/matlab-startup-folder.html}{``Matlab Startup Folder''} for more information.  

Also, a local shell command specified by \tt{localShellCommand} in \tt{montyConfig.m} will be executed before copying files to the server. This can be used to copy local library files into the working directory.}.


\begin{figure}[t]
 \lstinputlisting{../../var/montyConfig.m}
 \caption{\tt{montyConfig.m} as included in the distribution.  Must be modified for your server environment.  \label{fig:configRemote}}
\end{figure}


Your working directory might be \tt{\textasciitilde/myProject}, and it should contain the following:
\begin{verbatim}
var/                    %curve remove configuration directory 
var/montyConfig.m       %remote configuration 
exampleSimulation.m     %simulation class file
myData.mat              %your saved simulation data
\end{verbatim}
It might also include:
\begin{verbatim} 
startup.m               %sets library path
myLib/                  %your libraries 
@mySimulation/   %class in a directory
\end{verbatim}

(4) \emph{Keychain}  Use keychain to avoid typing passwords when you connect to the server.  Using public key cryptography, your local machine has a private key, and the server has the public key.  To set up keychain on Mac~OS and Linux, perform the following on the \tt{local} machine and \tt{remote} machine:
\begin{verbatim}
local$ ssh-keygen 
\end{verbatim}
Accept the default location.  To use no passphase, press enter 2 times.  
\begin{verbatim}
local$ scp .ssh/id_rsa.pub s1510000@vpcc:~/.ssh 
local$ ssh s1510000@vpcc
remote% cd .ssh
remote% cat id_rsa.pub >> authorized_keys
remote% chmod 700 ~/.ssh
remote% chmod 600 ~/.ssh/authorized_keys
\end{verbatim}
In this example, the username is \tt{s1510000} and the server name is \tt{vpcc}. By installing keychain with no passphrase, unauthorized access to your local machine might allow unauthorized access to the server. Additional software like keychain or Keychain Access can help manage passphrases.

In addition, \montylibrary provides the following commands:

\begin{description}
\item[\tt{monty.qstat}] lists jobs owned by you on the remote server, by running \tt{\% qstat -u username}.

\item[\tt{monty.qdel(23456)}] deletes job 23456 on the remote server, by running  \tt{\% qdel 23456}.

\item[\tt{monty.fromRemote}] uses rsync to copy files from the server to the local machine.  This is for copying simulation results stored in files back to the local machine.  The rsync \tt{--update} option is used so that only files that have been changed, for example simulation results, will be copied.

\item[\tt{curve = monty.fromRemote('dataHamming')}] additionally attempts to load the file \tt{dataHamming.mat} and returns the contained \tt{curve}.  This file will be loaded even if it was not one of those copied from the server.

\item[\tt{monty.ping}]  tests that commands can be run on the remote server, and:
\begin{lstlisting}
Server is vpcc and PWD is /home/username 
\end{lstlisting}
indicates that running pwd on the remote server was successful.

\item[\tt{monty.mkdirVar}] will copy the example config file to \tt{var/montyConfig}.

\item[\tt{monty.remoteShell}] runs a shell command on the server in the working directory.   For example, \tt{monty.remoteShell('qstat -H 128674')} displays the status of PBS job 128674.   Equivalent to \tt{monty.ping}, a simple check of your configuration setting is \tt{monty.remoteShell('pwd')}, which should return the working directory on the server.  Stop and delete job 128674 with \tt{monty.remoteShell('qdel 128674')}.  List all \tt{username} jobs with \tt{monty.remoteShell('qstat -u username')}.

\item[\tt{monty.sendMail}] can send you an email message, for example when your simulation completes.  See \tt{help monty.sendMail} for more details.

\end{description}

\section{Threshold Search}

Preliminary support has been added for threshold searches, for example, the noise threshold for LDPC codes.  The function interface is similar, making threshold searches straightforward, although there is no parallel processing or statistical reliablity aspects.  The threshold search performs a simple binary search, and your function must provide a 0/1 or \tt{false}/\tt{true} value in \tt{curve.check} to indicate if the result is below or above the threshold.

In your \tt{myThresholdSearch} file, the first three lines should be of the form:
\begin{lstlisting}
curve = curveEnter(mfilename,varargin);
[curve,thresholdInitReturn] ...
             = curveEnterThreshold(curve,searchParam,low,high,res);
if thresholdInitReturn; return; end 
\end{lstlisting}
The first line is as before.  In the second line, \tt{curveEnterThreshold} has five required parameters.  After \tt{curve},  \tt{searchParam} is parameter that is being searched, and it must be a field.  \tt{low} and \tt{high} are the initial low and high search points.  \tt{res} is the resolution that must be achieved, that is, the threshold search will stop when the gap between high and low is less than \tt{res}.  For example,
\begin{lstlisting}
[curve,thresholdInitReturn] ...
                  = curveEnterThreshold(curve,'SNRdb',0,2,0.05);
\end{lstlisting}
shows the parameter search is \tt{curve.SNRdb}, the initial low value is 0, the initial high value is 2, and the resolution is 0.05.  

Your function must set the value \tt{curve.check} to 0 or \tt{false} when the search parameter is equal to \tt{low}.  And, it must set  the value \tt{curve.check} to 1 or \tt{true} when the search parameter is equal to \tt{high}.  A simple strategy is to set extreme inital values, since the search is binary.

To perform a search, setup the threshold search then run \tt{curveThresholdSearch}.  For example, if your function is \tt{myThresholdSearch} and the data file is \tt{threshData}, and the parameter \tt{Ns} should be 10000 then:
\begin{lstlisting}
monty.generate('threshData','myThresholdSearch','Ns',10000);
curveThresholdSearch('threshData');
\end{lstlisting}
Once the threshold search is complete:
\begin{lstlisting}
load threshData
[low,high] = curveThresholdFindLowHigh(curve) 
\end{lstlisting}
will give the \tt{low} and \tt{high} limits of the search.

\section{Examples  \label{sec:examples} }

This section gives examples of \montylibrary.

\emph{Example} Perform a simulation with a stopping condition of 10 word errors.  After the simulation is complete, the results look promising, but you want to increase the statistical reliability by increasing to 100 word errors.  Update all \tt{montyContinue} fields at the same time:
\begin{lstlisting}
>> monty.generate('dataHamming',exampleSimulation,...
       'ebnodb',{1 2 3 4},'montyContinue','curve.nWordError < 10')
>> monty.run('dataHamming')
 ...
>> load dataHamming.mat
>> [curve(:).montyContinue] = deal('curve.nWordError < 100') 
>> save dataHamming curve
>> monty.run('dataHamming')
\end{lstlisting}

\emph{Example} After 24 hours of simulation time, SNRs of 4 dB and 5 dB gave no errors at all.   These should be removed from the simulation so that the server can spend more time on SNRs of 0 dB, 1 dB, 2 dB and 3 dB.  Assuming \curve array has 6 elements:
\begin{lstlisting}
load myData.mat
curve = curve(1:4);
save myData curve
monty.run('myData')
\end{lstlisting}
Or:
\begin{lstlisting}
load myData.mat
curve(5).montyContinue = false;
curve(6).montyContinue = false;
save myData curve
monty.run('myData')
\end{lstlisting}
The first form deletes the data, the second form keeps the data but will never be attempted since the continue condition is false.

%\section{FAQ \label{sec:faq}}
%
%\begin{description}
%\item[Q] I got the error message ``Too many input arguments'' even though the number of input arguments is correct.  The error occurs at a function I wrote.
%\item[A]  When parallel processing, elements of structures cannot be passed directly as input arguments, that is, \tt{var = ebnodb2var(curve.ebnodb)} generates the error.  The solution is to pass the value using a new variable:
%\begin{lstlisting}
%ebnodb = curve.ebnodb;
%var    = HC.ebnodb2var(ebnodb);
%\end{lstlisting}
%\item[Q] I got the message ``the variable \tt{curve} in \tt{parfor} cannot be classified''.  
%\item[A] This might mean you need to change \tt{curve} to \tt{curve(worker)}.
%\end{description}


\section{Reference}

The superclass \tt{monty} contains several properties:
\begin{itemize}
\item  \tt{monty.montyContinue} which is discussed above.
\item  \tt{monty.startTime}  The time the current simulation began, as indicated by the matlab \tt{clock} function.  The default value is empty.  After the simulation finishes, it is set to empty. 
\item  \texttt{sys.elapsedTime} The accumulated time that has been spent on this simulation.   In parallel simulations, the time on all processors is counted only once.
\item  \texttt{sys.workInterval} Time in seconds to simulate on this \curve, before moving to the next one.  The default value is 1200 seconds (20 minutes).
\item  \texttt{sys.inputs}  %When a simulation is initialized using \tt{monty.generate}, the original input arguments are stored here.  This is useful for starting a new simulation with the same or similar parameters.
\end{itemize}
The following properties deal with parallel processing:
\begin{itemize}
\item  \tt{monty.numberOfWorkers}  %Number of workers provided by the system, as reported by \tt{gcp}. 
\item  \tt{monty.montyAddField, monty.montyConcatField}  Cell arrays of strings, indicating the combining rule for each \curve property.
\end{itemize}

\section{\tt{exampleSimulation.m}}

\begin{figure}
 \lstinputlisting[linerange=1-48,breaklines=false]{../../exampleSimulation.m}
 \caption{Lines 1--47 of \tt{exampleSimulation.m} \label{fig:sim1}}
\end{figure}

\begin{figure}
 \lstinputlisting[firstline=49,firstnumber=48,breaklines=false]{../../exampleSimulation.m}
 \caption{Lines 48--96 of \tt{exampleSimulation.m} \label{fig:sim2}}
\end{figure}

\end{document}