classdef codeSimulation < monty
    properties
        %simulation parameters and their default or initial values
        
        %inputs
        ebnodb        = 7;
        code          = LDPC;
        
        %outputs
        nBitError     = 0;
        nWordError    = 0;
        nWord         = 0;
        
        %simulation parameters without defaults
        varChan;
        ber;
        wer;
    end
    
    properties (Hidden)
        %values that are computed on initialization, but do not need to be
        %displayed.
    end
    
    methods
        function curve = codeSimulation(varargin)
            %continue condition: if string evaluates true, then the
            %simulation will continue.  Otherwise, the simulation stops
            curve.montyContinue    = 'curve.nWordError < 10';
            curve.montyAddField    = {'nBitError','nWordError','nWord'};
            curve.montyConcatField = { };

            %process the input arguments, for example allows
            %codeSimulation('code',C) to override default 'code' value
            curve = curve.inputArguments(varargin{:});
        end
        
        function curve = processInputArguments(curve,varargin)
            assert(mod(length(varargin),2) == 0,'Each name requires an value')
            for ii = 1:length(varargin)/2
                assert(ischar(varargin{2*ii-1}),'Names must be strings of object properties');
                curve.(varargin{2*ii-1}) = varargin{2*ii};
            end
        end
            
            
        function curve = simulate(curve)
            
            %SIMULATION CORE
            curve.varChan = curve.code.ebnodb2var(curve.ebnodb);
            while (curve.montyErrorLimit)
                x = curve.code.encoder;
                z = randn(size(x)) * sqrt(curve.varChan);
                y = x + z;
                
                xhat = curve.code.decoder(y,curve.varChan);
                
                ne                = length(find(xhat ~= x));
                curve.nBitError   = curve.nBitError + ne;
                curve.nWordError  = curve.nWordError + (ne > 0);
                curve.nWord       = curve.nWord + 1;
                if ne > 0
                    fprintf('word %10d   number of bit errors = %d\n',curve.nWord,ne);
                end
            end
            
            %Convenience calculations
            curve.ber = curve.nBitError / (curve.nWord * curve.code.n);
            curve.wer = curve.nWordError / curve.nWord ;
        end
    end
    
end