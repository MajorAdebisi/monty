function config = montyConfig

config.servername    = '150.65.182.40';
config.username      = 's1510000';
config.home          = '/home/s1510000';
config.remoteDir     = '~/myProject';

%ssh-keygen is recommended for password handling. See documentation
%for configuration.

%true: use PBS job scheduling software to run Matlab on remote server
%false: run Matlab directly on the remote server
config.usePBS        = true;

%PBS queue class depends on server configuration
%typical examples include 'TINY' 'SINGLE' 'SMALL' 'MEDIUM' 'LARGE'
%config.pbsQueueClass = 'SINGLE'

%local shell command executed before upload to remote server
%config.localShellCommand = 'rm -rf myLib ; cp -R ~/myLib .';

%For Windows, local machine uses putty for remote execution
%config.puttyPath     = 'C:\Users\Robert\putty.exe'
